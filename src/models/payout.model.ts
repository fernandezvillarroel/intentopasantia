import {Entity, model, property} from '@loopback/repository';

@model()
export class Payout extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  NIT?: number;

  @property({
    type: 'number',
  })
  discount?: number;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'number',
  })
  amount?: number;

  @property({
    type: 'date',
  })
  date?: string;


  constructor(data?: Partial<Payout>) {
    super(data);
  }
}

export interface PayoutRelations {
  // describe navigational properties here
}

export type PayoutWithRelations = Payout & PayoutRelations;
