import {Entity, model, property} from '@loopback/repository';

@model()
export class Intento1 extends Entity {
  @property({
    type: 'string',
  })
  name?: string;

  @property({
    type: 'string',
  })
  city?: string;


  constructor(data?: Partial<Intento1>) {
    super(data);
  }
}

export interface Intento1Relations {
  // describe navigational properties here
}

export type Intento1WithRelations = Intento1 & Intento1Relations;
