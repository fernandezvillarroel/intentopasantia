import {Entity, model, property} from '@loopback/repository';

@model()
export class User1 extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  Name?: string;

  @property({
    type: 'string',
  })
  Email?: string;

  @property({
    type: 'number',
  })
  CI?: number;

  @property({
    type: 'number',
  })
  PhoneNumber?: number;


  constructor(data?: Partial<User1>) {
    super(data);
  }
}

export interface User1Relations {
  // describe navigational properties here
}

export type User1WithRelations = User1 & User1Relations;
