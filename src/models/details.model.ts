import {Entity, model, property} from '@loopback/repository';

@model()
export class Details extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;


  constructor(data?: Partial<Details>) {
    super(data);
  }
}

export interface DetailsRelations {
  // describe navigational properties here
}

export type DetailsWithRelations = Details & DetailsRelations;
